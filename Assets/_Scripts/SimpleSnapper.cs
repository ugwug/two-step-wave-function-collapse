using HexGrid;
using HexTileGrid;
using System.Collections;
using System.Collections.Generic;
using TGS;
using UnityEditor;
using UnityEngine;
using static HexGrid.HexGrid;

[DisallowMultipleComponent]
[ExecuteInEditMode]
[RequireComponent(typeof(TerrainGridSystem))]
public class SimpleSnapper : MonoBehaviour
{
    [SerializeField, HideInInspector]
    TerrainGridSystem tgs = null;

    [SerializeField]
    string gridSnapLayerName = "SnapHexGrid";

    private void OnEnable()
    {
        EditorApplication.update += Update;

        if (tgs == null)
        {
            GetGrid();
        }
    }

    private void OnDisable()
    {
        EditorApplication.update -= Update;
    }

    private void OnDestroy()
    {
        EditorApplication.update -= Update;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (EditorApplication.isPlaying || tgs == null)
        {
            return;
        }

        GameObject[] selectedGameObjects = Selection.gameObjects;

        if (selectedGameObjects != null && selectedGameObjects.Length > 0)
        {
            for (int i = 0; i < selectedGameObjects.Length; ++i)
            {
                SnapSelectedObjectToGrid(selectedGameObjects[i]);
            }
        }
    }

    private void SnapSelectedObjectToGrid(GameObject selectedGameObject)
    {
        if (selectedGameObject != null && PrefabUtility.IsPartOfAnyPrefab(selectedGameObject))
        {
            selectedGameObject = PrefabUtility.GetOutermostPrefabInstanceRoot(selectedGameObject);
        }

        if (selectedGameObject == null)
        {
            return;
        }

        string layerName = gridSnapLayerName;
        if (selectedGameObject.layer == LayerMask.NameToLayer(layerName) || string.IsNullOrEmpty(layerName))
        {
            //Warning: might only works without territories 
            selectedGameObject.transform.position = tgs.CellGetPosition(
                                            tgs.CellGetAtPosition(selectedGameObject.transform.position, true).index);
        }

    }

    private void GetGrid()
    {
        if (TryGetComponent(out TerrainGridSystem grid))
        {
            this.tgs = grid;
        }
    }

}
