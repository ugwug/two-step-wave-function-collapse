using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Animations;
using UnityEngine;

public enum BIOME
{
    Root,
    ForestSparse,
    ForestDense,
    Forest,
    Meadow,
    Mountain,
    River,
    Lake,
    None
}

[System.Serializable]
public class Node : MonoBehaviour
{
    [SerializeField]
    public GameObject tile;

    public BIOME biome;
    public bool isLowestAbstraction;
    public float weight;
    public List<Node> children;

    public Node(BIOME b, bool l, float w)
    {
        isLowestAbstraction = l;
        biome = b;
        weight = w;
    }
}
