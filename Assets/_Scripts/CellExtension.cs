using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace TGS
{    public enum CELL_SIDE_POINTY_TOP
    {
        TopLeft = 0,
        TopRight = 2,
        BottomRight = 3,
        BottomLeft = 5,
        Left = 6,
        Right = 7
    }

    public partial class Cell : AdminEntity
    {
        public int tileID = -1;

        public List<Pattern> possiblePatterns;
    }
}
