using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using TGS;
using TGS.ClipperLib;
using UnityEngine;


public class SimpleGenerator : MonoBehaviour
{
    TerrainGridSystem tgs;

    [SerializeField]
    GameObject[] tiles = null;

    [SerializeField]
    int repeats = 10;

    float startTime;
    float endTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.realtimeSinceStartup;

        tgs = TerrainGridSystem.instance;
        FillCells();
        DisplayCells();

        endTime = Time.realtimeSinceStartup - startTime;
        Debug.Log(endTime.ToString());
    }

    void FillCells()
    {
        for (int i = 0; i < repeats; i++)
        {
            int a = UnityEngine.Random.Range(0, tiles.Length);
            tgs.cells[i].tag = a;
            tgs.CellSetColor(tgs.cells[i], UnityEngine.Color.green);
        }
    }

    // TODO: include this in the chunk system, to only display cells inside render distance
    void DisplayCells()
    {
        foreach (var cell in tgs.cells)
        {
            //Instantiate(tiles[cell.tag], tgs.CellGetPosition(cell, true), Quaternion.identity);
        }
    }
}
