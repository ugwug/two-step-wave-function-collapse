using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using TGS;
using UnityEngine;
using UnityEngine.SceneManagement;

public struct Pattern
{
    public int center;
    public int topRight;
    public int right;
    public int bottomRight;
    public int bottomLeft;
    public int left;
    public int topLeft;

    public Pattern(int c, int tr, int r, int br, int bl, int l, int tl)
    {
        this.center = c;
        this.topRight = tr;
        this.right = r;
        this.bottomRight = br;
        this.bottomLeft = bl;
        this.left = l;
        this.topLeft = tl;
    }


    public Pattern(int c, List<int> neighborTiles)
    {
        this.center = c;
        this.topRight = neighborTiles[0];
        this.right = neighborTiles[1];
        this.bottomRight = neighborTiles[2];
        this.bottomLeft = neighborTiles[3];
        this.left = neighborTiles[4];
        this.topLeft = neighborTiles[5];
    }

    public override string ToString()
    {
        string result = "Pattern " +
                        "center: " + this.center +
                        "tr " + this.topRight +
                        "r" + this.right +
                        "br" + this.bottomRight +
                        "bl" + this.bottomLeft +
                        "l" + this.left +
                        "tl" + this.topLeft;
        return result;
    }
    public bool IsFull()
    {
        if (center != -1
            && topRight != -1
            && right != -1
            && bottomRight != -1
            && bottomLeft != -1
            && left != -1
            && topLeft != -1
            )
        {
            return true;
        }
        return false;
    }

    public int getSide(int side)
    {
        switch (side)   // is is the listIndex from tgs.CellGetNeighbors
        {
            case 0: return center;
            case 1: return topRight;
            case 2: return right;
            case 3: return bottomRight;
            case 4: return bottomLeft;
            case 5: return left;
            case 6: return topLeft;
            default: return -1;
        }
    }
    public bool Equals(int a, int b)
    {
        if (a == -1 || b == -1 || a == b)
        {
            return true;
        }
        return false;
    }

    public bool MatchesRotation(Pattern toMatch)
    {
        Pattern thisPattern = new Pattern(this.center, this.topRight, this.right, this.bottomRight, this.bottomLeft, this.left, this.topLeft);
        for (int i = 0; i < 6; i++)
        {
            if (thisPattern.PatternMatch(toMatch))
            {
                return true;
            }
            thisPattern.RotateMeClockwise(out thisPattern);
        }
        return false;
    }

    public bool PatternMatch(Pattern toMatch)
    {
        if (   Equals(center,       toMatch.center)
            && Equals(topRight,     toMatch.topRight)
            && Equals(right,        toMatch.right)
            && Equals(bottomRight,  toMatch.bottomRight)
            && Equals(bottomLeft,   toMatch.bottomLeft)
            && Equals(left,         toMatch.left)
            && Equals(topLeft,      toMatch.topLeft))
        {
            return true;
        }
        return false;
    }

    public bool PatternMatchRotation(Pattern toMatch, out Pattern rotatedPattern)
    {
        Pattern rotated = new Pattern(this.center, this.topRight, this.right, this.bottomRight, this.bottomLeft, this.left, this.topLeft);
        for (int i = 0; i < 6; i++)
        {
            if (PatternMatch(toMatch))
            {
                rotatedPattern = rotated;
                return true;
            }
            RotateMeClockwise(out rotated);
        }
        rotatedPattern = rotated;
        return false;
    }

    public bool PatternMatchRotation(Pattern toMatch)
    {
        Pattern rotated = new Pattern(this.center, this.topRight, this.right, this.bottomRight, this.bottomLeft, this.left, this.topLeft);
        for (int i = 0; i < 6; i++)
        {
            if (PatternMatch(toMatch))
            {
                return true;
            }
            RotateMeClockwise(out rotated);
        }
        return false;
    }

    public void RotateMeClockwise(out Pattern rotatedPattern)
    {
        int oldTopRight = topRight;
        topRight = topLeft;
        topLeft = left;
        left = bottomLeft;
        bottomLeft = bottomRight;
        bottomRight = right;
        right = oldTopRight;
        rotatedPattern = new Pattern(this.center, this.topRight, this.right, this.bottomRight, this.bottomLeft, this.left, this.topLeft);
    }
}

[System.Serializable]
public struct LayerFields
{
    public int generatedTerrain;
    public int inputTerrain;
    public int DAG;
}


public class WFCGenerator : MonoBehaviour
{
    public const int                    HEXAGONAL_DIRECTIONS = 6;
    [SerializeField] LayerFields        layers;
    [SerializeField] GameObject[]       tiles = null;    
    [SerializeField] int                repeats = 100;    
    [SerializeField] bool               debug = false;
    [SerializeField] int                startCell = 525;        //TODO: set to center of grid
    TerrainGridSystem                   tgs;
    bool[][][]                          adjacencyMatrices;
    bool[][]                            isAdjacentTo;
    int                                 currentCellID;
    List<int>                           inputCells;
    Node[]                              territoryDAGs;
    List<Pattern>                       patterns;              
    List<int>                           tileIDs;                // according to GameObject[] tiles  
    List<int>                           stackCellIDs;
    float                               startTime;
    float                               endTime;

    void Start()
    {
        // Get a reference to Terrain Grid System's API
        Generate();
        DisplayCells(ReadInput(layers.generatedTerrain));
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space)) {
            Observe();
            Propagate();
            DisplayNewCell(ReadInput(layers.generatedTerrain));
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Observe();
            Propagate();
            VisualizeStack();
            DisplayNewCell(ReadInput(layers.generatedTerrain));
        }
    }

    private void VisualizeStack()
    {
        foreach (var cellID in stackCellIDs)
        {
            tgs.CellFadeOut(tgs.cells[cellID], UnityEngine.Color.green, 0.2f);
        }
    }

    private void DisplayNewCell(GameObject terrain)
    {
        Cell cell = tgs.cells[currentCellID];
        GameObject go = Instantiate(tiles[cell.tileID], tgs.CellGetPosition(cell, true), Quaternion.identity);
        go.transform.SetParent(terrain.transform);
    }


    void Generate()
    {
        startTime = Time.realtimeSinceStartup;
        InitGenerator();
        Analyze();
        InitGrid();

        //TODO iterate over tgs.cells.size instead
        if (repeats > tgs.cells.Count) repeats = tgs.cells.Count;
        for (int i = 0; i < repeats; i++)
        {
            Observe();
            Propagate();
        }
        tgs.showCells = false;
        endTime = Time.realtimeSinceStartup - startTime;
        Debug.Log(endTime.ToString());
        return;

    }

    // TODO: embedd this in the chunk system to only display cells inside render distance
    void DisplayCells(GameObject terrain)
    {
        foreach (var cell in tgs.cells)
        {
            if (cell.tileID != -1)
            {
                GameObject go = Instantiate(tiles[tileIDs.IndexOf(cell.tileID)], tgs.CellGetPosition(cell, true), Quaternion.identity);
                go.transform.SetParent(terrain.transform);
            }
        }
    }


    private void InitGenerator()
    {
        tgs                     = TerrainGridSystem.instance;
        tileIDs                 = InitTileIDs();
        patterns                = new List<Pattern>();
        inputCells              = new List<int>();
        stackCellIDs            = new List<int>();
        territoryDAGs           = new Node[tgs.territories.Count];

        tgs.OnCellClick += Tgs_OnCellClick;
    }

    private List<int> InitTileIDs()
    {
        List<int> list = new List<int>();
        foreach (var go in tiles)
        {
            list.Add(GetTileID(go));
        }
        return list;
    }


    private void Analyze()
    {
        LinkInputMapToGrid(ReadInput(layers.inputTerrain));

        LinkDAGToTerritories(ReadInput(layers.DAG));

        CreatePatterns();

        //CalculateAdjacencyMatrices();

        CleanUpGrid(ReadInput(layers.inputTerrain));

        DebugAnalyzeStep();
    }

    private void LinkInputMapToGrid(GameObject terrain)
    {
        if (terrain == null) return;
        for (int child = 0; child < terrain.transform.childCount; child++)
        {
            Transform tile = terrain.transform.GetChild(child);
            int tileID = GetTileID(tile.gameObject);
            if (tileID == -1) continue;
            int cellID = tgs.CellGetIndex(tgs.CellGetAtPosition(tile.position, true));
            inputCells.Add(cellID);
            tgs.cells[cellID].tileID = tileID;
        }

        if (inputCells == null || inputCells.Count == 0)
        {
            Debug.LogError("we're missing an input!");
            return;
        }
    }

    private void LinkDAGToTerritories(GameObject dag)
    {
        // TODO: check if DAG is valid

        // fill TerritoryNodes
        for (int i = 0; i < tgs.territories.Count; i++)
        {
            Node root = dag.GetComponent<Node>();
            territoryDAGs[i] = FillDAG(root, dag);
        }
    }
    // recursive function that returns a node with lowest level of abstraction.
    // selection is random and based on weights.
    private Node FillDAG(Node parentNode, GameObject dag)
    {
        // if node is lowest abstraction (has only tiles as children, no nodes): return it
        if (parentNode.isLowestAbstraction)
        {
            return parentNode;
        }

        // else: go deeper
        float rng = UnityEngine.Random.Range(0.0f, 1.0f);
        float lastWeight = 0.0f;
        for (int i = 0; i < dag.transform.childCount; i++)
        {
            GameObject child = dag.transform.GetChild(i).gameObject;
            float childWeight = child.GetComponent<Node>().weight + lastWeight;
            if (rng < childWeight)
            {
                Node node = child.GetComponent<Node>();
                return FillDAG(node, child);
            }
            lastWeight = childWeight;
        }

        // shouldn't happen
        Debug.LogError("nodes aren't properly setup!");
        return parentNode;
    }

    private void CreatePatterns()
    {
        foreach (var cell in inputCells)
        {
            Pattern pattern;
            if (MakeFullPattern(cell, out pattern))
            {
                // TODO(?): check whether this pattern is already in patterns
                patterns.Add(pattern);
            }
        }
    }

    private void CleanUpGrid(GameObject terrain)
    {
        // remove randomly placed tiles that don't belong to any terrain
        foreach (var go in GameObject.FindGameObjectsWithTag("tile"))
        {
            go.SetActive(false);
        }

        // remove all tiles of current terrain 
        for (int child = 0; child < terrain.transform.childCount; child++)
        {
            if (terrain.transform.GetChild(child).gameObject.tag == "tile")
            {
                terrain.transform.GetChild(child).gameObject.SetActive(false);
            }
        }
    }


    private void InitGrid()
    {
        stackCellIDs.Clear();
        stackCellIDs.Add(startCell);
        foreach (var cell in tgs.cells)
        {
            cell.possiblePatterns = new List<Pattern>();
            int territoryIndex = tgs.CellGetTerritoryIndex(cell.index);
            if (territoryIndex == -1)
            {
                Debug.LogError("cell has no territory");
                tgs.CellFadeOut(cell.index, UnityEngine.Color.red, 0.2f);
            }
            List<Node> currentBiomeTiles = territoryDAGs[tgs.CellGetTerritoryIndex(cell.index)].children;
            List<int> biomeTileIDs = new List<int>();
            for (int i = 0; i < currentBiomeTiles.Count; i++)
            {
                biomeTileIDs.Add(GetTileID(currentBiomeTiles[i].gameObject));
            }
            foreach (var pattern in patterns)
            {
                if (biomeTileIDs.Contains(pattern.center))
                {
                    cell.possiblePatterns.Add(pattern);
                }
            }
            cell.tileID = -1;
        }
    }

    private void Observe()
    {
        Choose();

        Collapse();

        PopStack();
    }

    private void Choose()
    {
        // return if stack is empty
        if (stackCellIDs.Count == 0)
        {
            currentCellID = -1;
            Debug.LogError("stack is empty");
            return; 
        }

        // TODO: find most constrained cell, instead of first uncollapsed cell
        for(int i = 0; i < stackCellIDs.Count; i++)
        {
            if (tgs.cells[stackCellIDs[i]].tileID == -1)    // TODO: this check should not be necessary
            {
                currentCellID = stackCellIDs[i];
                return;
            }
        }

        currentCellID = -1;
        Debug.LogError("no free cell found");
    }

    private void Collapse()
    {
        if (currentCellID == -1)
        {
            Debug.LogError("no uncollapsed Cells left");
            return;
        }

        // TODO: outsource this
        // determine overlap of tgs.cells[currentCell].possiblePatterns with children of current biome node

        // 1. determine current biome node
        List<Node> currentBiomeTiles = new List<Node>();
        float weightSum = 0;
        for (int i = 0; i < territoryDAGs[tgs.CellGetTerritoryIndex(currentCellID)].gameObject.transform.childCount; i++)
        {
            currentBiomeTiles.Add(territoryDAGs[tgs.CellGetTerritoryIndex(currentCellID)].gameObject.transform.GetChild(i).gameObject.GetComponent<Node>());
        }
        List<int> biomeTileIDs = new List<int>();
        for (int i = 0; i < currentBiomeTiles.Count; i++)
        {
            biomeTileIDs.Add(GetTileID(currentBiomeTiles[i].gameObject)); // TODO optimize?
        }

        // 3. overlap cell's possibleTileIDs with biomeTileIDs
        List<int> possibleTileIDs = new List<int>();
        List<Node> possibleTiles = new List<Node>();
        foreach (var pattern in tgs.cells[currentCellID].possiblePatterns)
        {
            if (biomeTileIDs.Contains(pattern.center))
            {
                if (!possibleTileIDs.Contains(pattern.center))
                {
                    Node n = currentBiomeTiles[biomeTileIDs.IndexOf(pattern.center)];
                    possibleTileIDs.Add(pattern.center);
                    possibleTiles.Add(n);
                    weightSum += n.weight;
                }
            }
        }

        string line = "possible tiles: ";
        foreach (var tile in possibleTileIDs)
        {
            line += tile.ToString() + ",";
        }
        //Debug.Log(line);

        // set current cells tileID (if possible)
        if (possibleTileIDs.Count <= 0)
        {
            Debug.LogWarning("current Cell has no possible tiles");
            tgs.cells[currentCellID].tileID = biomeTileIDs[0];          // set biome standard tile to prevent holes
            return;
        } else
        {
            float rng = UnityEngine.Random.Range(0.0f, weightSum);
            float lastWeight = 0.0f;
            for (int i = 0; i < possibleTiles.Count; i++)
            {
                float childWeight = possibleTiles[i].weight + lastWeight;
                if (rng < childWeight)
                {
                    tgs.cells[currentCellID].tileID = possibleTileIDs[i];
                    //tgs.CellSetColor(tgs.cells[currentCellID], UnityEngine.Color.green);
                    //Debug.Log("place tile with ID " + possibleTileIDs[i]);
                    break;
                }
                lastWeight = childWeight;
            }
            //Debug.Log("possible tiles count: " + possibleTileIDs.Count + ", rng:" + rng + ", weighted sum: " + weightSum);
        }
    }


    private void Propagate()
    {
        AddNewNeighbours();

        UpdateUncollapsedNeighboursPatterns(); //TODO make it recursive
    }

    private void AddNewNeighbours()
    {
        foreach (var neighborCell in tgs.CellGetNeighbours(tgs.cells[currentCellID]))
        {
            int neighborID = tgs.CellGetIndex(neighborCell);
            if (!stackCellIDs.Contains(neighborID) && tgs.cells[neighborID].tileID == -1)
            {
                stackCellIDs.Add(neighborID);
            }
        }
    }

    private void UpdateUncollapsedNeighboursPatterns()
    {
        // get neighbourCellIDs
        List<int> neighbourCellIDs = new List<int>();
        foreach (var neighbourCell in tgs.CellGetNeighbours(currentCellID))
        {
            if (neighbourCell.tileID == -1)
            {
                neighbourCellIDs.Add(neighbourCell.index);
            }
        }

        //Debug.Log("current cell ID " + currentCellID + " ----------------------------------- ");

        foreach (var neighbourCellID in neighbourCellIDs)
        {
            // make currentActualPattern arround neighbourCell
            Pattern currentActualPattern;
            MakeFullPattern(neighbourCellID, out currentActualPattern);

            List<Pattern> currentPossiblePatterns = tgs.cells[neighbourCellID].possiblePatterns;
            // remove all possible patterns that don't match with the actual pattern
            for (int i = 0; i < currentPossiblePatterns.Count; i++)
            {
                if (!currentActualPattern.MatchesRotation(currentPossiblePatterns[i]))
                {
                    //DebugPattern(currentPossiblePatterns[i], "removed pattern " + i + ": ");
                    tgs.cells[neighbourCellID].possiblePatterns.Remove(currentPossiblePatterns[i]);
                    i--;
                }
            }
        }
    }

    private void PopStack()
    {
        stackCellIDs.Remove(currentCellID);
    }

    private bool MakeFullPattern(int cellID, out Pattern result)
    {
        List<int> neighbourCellIDs = new List<int>();
        foreach (var cell in tgs.CellGetNeighbours(cellID)) //TODO write custom get neighbors function that checks whether cell actually has 6 neighbors
        {
            neighbourCellIDs.Add(tgs.CellGetIndex(cell));
        }

        if (neighbourCellIDs.Count != 6)
        {
            Debug.Log("not enough neighbors");
            result = new Pattern();
            return false;
        }
        int center  = tgs.cells[cellID].tileID;
        int tr      = tgs.cells[neighbourCellIDs[0]].tileID;
        int r       = tgs.cells[neighbourCellIDs[1]].tileID;
        int br      = tgs.cells[neighbourCellIDs[2]].tileID;
        int bl      = tgs.cells[neighbourCellIDs[3]].tileID;
        int l       = tgs.cells[neighbourCellIDs[4]].tileID;
        int tl      = tgs.cells[neighbourCellIDs[5]].tileID;
        result      = new Pattern(center, tr, r, br, bl, l, tl);
        if (result.IsFull())
        {
            return true;
        }
        return false;
    }
    
    private int GetTileID(GameObject gameObject)
    {
        try
        {
            return int.Parse(gameObject.name.Split('_')[1].Split(' ')[0]);
        }
        catch
        {
            Debug.LogError("Error! Prefab naming convention is broken!" + gameObject.name);
            return 0;
        }
    }

    private GameObject ReadInput(int layer)
    {
        foreach (var go in SceneManager.GetActiveScene().GetRootGameObjects())
        {
            if (go.layer == layer)
            {
                return go;
            }
        }
        Debug.LogError("the scene is not properly setup! Please make sure, that all specified game objects are " +
                       "listed in the hierarchy.");

        return null;
    }

    static T CreateJaggedArray<T>(params int[] lengths)
    {
        return (T)InitializeJaggedArray(typeof(T).GetElementType(), 0, lengths);
    }

    static object InitializeJaggedArray(Type type, int index, int[] lengths)
    {
        Array array = Array.CreateInstance(type, lengths[index]);
        Type elementType = type.GetElementType();

        if (elementType != null)
        {
            for (int i = 0; i < lengths[index]; i++)
            {
                array.SetValue(
                    InitializeJaggedArray(elementType, index + 1, lengths), i);
            }
        }

        return array;
    }

    private void Tgs_OnCellClick(TerrainGridSystem tgs, int cellIndex, int buttonIndex)
    {
        Debug.Log(territoryDAGs[cellIndex].biome.ToString());
    }

    private void DebugPattern(Pattern p, string s)
    {
        Debug.Log(s);
        Debug.Log("pattern: " + p.center
                                 + p.topRight
                                 + p.right
                                 + p.bottomRight
                                 + p.bottomLeft
                                 + p.left
                                 + p.topLeft);
    }
    private void DebugAnalyzeStep()
    {
        if (!debug) { return; }

        // pattern
        foreach (var pattern in patterns)
        {
            Debug.Log("pattern:  "   + pattern.center
                                     + pattern.topRight
                                     + pattern.right
                                     + pattern.bottomRight
                                     + pattern.bottomLeft
                                     + pattern.left
                                     + pattern.topLeft);
        }
    }
}
