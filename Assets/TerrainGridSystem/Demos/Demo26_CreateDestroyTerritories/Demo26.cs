using System.Collections;
using System.Collections.Generic;
using TGS;
using UnityEngine;

namespace TGS
{
    public class Demo26 : MonoBehaviour
    {
        TerrainGridSystem tgs;
        Territory territory;
        
        void Start()
        {
            tgs = TerrainGridSystem.instance;

            tgs.OnCellClick += OnCellClick;
            tgs.OnTerritoryClick += OnTerritoryClick;
        }

        private void OnTerritoryClick(TerrainGridSystem tgs, int territoryIndex, int buttonIndex) {
            if (buttonIndex == 1) {
                tgs.TerritoryDestroy(territoryIndex);
                territory = null;
            }
        }

        private void OnCellClick(TerrainGridSystem tgs, int cellIndex, int buttonIndex)
        {
            if (buttonIndex != 0) return;

            if (territory == null) {
                // Create territory with one cell at click position
                territory = tgs.TerritoryCreate(cellIndex);
                territory.fillColor = Color.blue;
                return;
            }

            // Check if clicked cell is adjacent to territory
            int territoryIndex = tgs.TerritoryGetIndex(territory);
            if (tgs.CellIsAdjacentToTerritory(cellIndex, territoryIndex)) {
                tgs.CellSetTerritory(cellIndex, territoryIndex);
            }
        }
    }
}
